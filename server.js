const mysql = require('mysql');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {cors: {origin: "*"}})

server.listen(8080, function () {
    console.log('Port: 8080');
});

function setStatus(s) {
    io.emit('status', s);
}

const
    con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "chat_db"
    });

con.connect(function (err) {
    if (err) throw err;
    io.on('connection', function (socket) {

        con.query('SELECT * FROM messages ORDER BY messages.id DESC LIMIT 100', function (err, res) {
            if (err) throw err;
            socket.emit('output', res);
        });

        socket.on('input', function (data) {
            let name = data.name;
            let message = data.message;
            let whiteSpacePattern = /^\s*$/;

            if (whiteSpacePattern.test(name) || whiteSpacePattern.test(message)) {
                setStatus('Message and name are required');
            } else {
                con.query(`INSERT INTO messages (name, message) values ("${name}", "${message}")`, function (err, result) {
                    if (err) throw err;
                    io.emit('output', [data])
                    setStatus({'message': 'Sent!', 'clear': true});
                });
            }

        });
    });
});